# Kata refacto to Clean Architecture

### Installation

```
yarn
```

### Launch server
```
- yarn build:watch 
- yarn start:watch
```

### Launch Postgres DataBase
```
- docker-compose up -d
```

### Kata
```
- To start the kata: https://gitlab.com/mickaelw/kata-refactoring-ca/tree/v1.0
- One possible solution: https://gitlab.com/mickaelw/kata-refactoring-ca/tree/v1.1
```

# Information:

### API endpoint
- http://localhost:8888/api/v1/books

### Database
- name: demo
- username: demo
- password: demo
