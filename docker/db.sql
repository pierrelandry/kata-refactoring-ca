create table book
(
  isbn     varchar(255) not null,
  title    varchar(255) not null,
  "authorId" varchar(255) not null
);

create unique index book_isbn_uindex on book (isbn);

alter table book
  add constraint book_pk primary key (isbn);

create table author
(
  id         varchar(255) not null,
  first_name varchar(255) not null,
  last_name  varchar(255) not null
);

create unique index author_id_uindex on author (id);

alter table author
  add constraint author_pk primary key (id);


alter table book
  add constraint book_authorId_id_fk foreign key ("authorId") references author on delete cascade;


insert into author (id, first_name, last_name)
values ('1', 'robert', 'martin');
insert into author (id, first_name, last_name)
values ('2', 'kent', 'beck');

insert into book (isbn, title, "authorId")
values ('1', 'clean code', '1');
insert into book (isbn, title, "authorId")
values ('2', 'clean architecture', '1');
insert into book (isbn, title, "authorId")
values ('3', 'test driven development by example', '2');
