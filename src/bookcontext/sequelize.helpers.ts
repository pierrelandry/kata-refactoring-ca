import Sequelize from 'sequelize'

export interface SequelizeDTO<D> {
    dataValues: D
}

const sequelize = new Sequelize('demo', 'demo', 'demo', {
    host   : 'localhost',
    dialect: 'postgres'
})

export {
    sequelize
}
