import Sequelize from 'sequelize'
import { sequelize } from '../sequelize.helpers'

const Author = sequelize.define('author', {
    id        : {
        primaryKey: true,
        type      : Sequelize.STRING
    },
    first_name: {
        type: Sequelize.STRING
    },
    last_name : {
        type: Sequelize.STRING
    }
}, {
    tableName : 'author',
    timestamps: false
})

export {
    Author
}
