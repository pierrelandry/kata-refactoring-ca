import Sequelize from 'sequelize'
import { sequelize } from '../sequelize.helpers'
import { Author } from './author'

const Book = sequelize.define('book', {
    isbn : {
        primaryKey: true,
        type      : Sequelize.STRING
    },
    title: {
        type: Sequelize.STRING
    }
}, {
    tableName : 'book',
    timestamps: false
})

Book.belongsTo(Author, { targetKey: 'id', as: 'author' })

export {
    Book
}