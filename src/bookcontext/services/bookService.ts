import { Author } from '../entities/author'
import { Book } from '../entities/book'
import { SequelizeDTO } from '../sequelize.helpers'

type AuthorDTO = SequelizeDTO<{
    id: string
    first_name: string
    last_name: string
}>

type BookDTO = SequelizeDTO<{
    isbn: string
    title: string
    author: AuthorDTO
}>

export const BookService = {
    findAll: Book.findAll({ include: [{ as: 'author', model: Author }] })
        .then((books: BookDTO[]) =>
            books.map(book => ({
                    ...book.dataValues,
                    author: book.dataValues.author.dataValues
                })
            )
        )
}
