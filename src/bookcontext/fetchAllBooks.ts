import { Request, Response } from 'express'
import { BookService } from './services/bookService'

const fetchAllBooks = () =>
    (req: Request, res: Response) => BookService.findAll.then(books => res.send(books))

export const bookContextRoutes = app => {
    app.get('/api/v1/books', fetchAllBooks())
}
