import { bookContextRoutes } from '../../bookcontext/fetchAllBooks'

const cors = require('cors')

export const rootRoute = app => {
    app.options('*', cors())
    bookContextRoutes(app)
}
